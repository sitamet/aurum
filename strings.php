<?php

/***Error messages***/

// Posting errors
define("S_SPAM", "Error: Your post looks like spam.");
define("S_NO_FILE", "Error: No file selected.");
define("S_NO_COM", "Error: New threads require a subject or comment.");
define("S_NO_BOARD", "Error: Specified board doesn't exist.");
define("S_NO_THREAD", "Error: Password incorrect.");
define("S_WRONG_PASS", "Error: Password incorrect.");
define("S_OLD_POST", "Error: You cannot delete a post this old.");

// Staff errors
define("S_MOD_EXISTS", "Error: User %s already exists.");
define("S_PERMISSION", "Error: Your access level isn't high enough to do that");

/*** FORM NAMES ***/

// Global
define("S_SUBMIT", "Submit");
define("S_SUN", "Sun");
define("S_MON", "Mon");
define("S_TUE", "Tue");
define("S_WED", "Wed");
define("S_THU", "Thu");
define("S_FRI", "Fri");
define("S_SAT", "Sat");
define("S_ANON", "Anonymous");
define("S_DELETE", "Delete");
define("S_RETURN", "Return");
define("S_LOGOUT", "Logout");
define("S_RESET", "Reset");
define("S_FILE_ONLY", "File Only");

// Posting
define("S_NAME", "Name");
define("S_SUB", "Subject");
define("S_COM", "Comment");
define("S_EMAIL", "Options");
define("S_VERIFY", "Verification");
define("S_FILE", "File");
define("S_TAG", "Tag");

// moderation
define("S_BAN", "Ban");
