<?php

/***Database Settings***/

// SQL Auth
define("SQL_TYPE", "mysql");
define("SQL_SERVER", "locolhost");
define("SQL_USER", "");
define("SQL_PASS", "");
define("SQL_DB", "");

// Tables
define("SQL_REPORTS", "reports");
define("SQL_MOD_USERS", "mod_users");
define("SQL_BANNED_USERS", "banned_users");
define("SQL_BANNED_USERS_BACKUP", "banned_users_backup");
define("SQL_BOARDS", "boards");
define("SQL_FILTERS", "filters");
define("SQL_IP_NOTES", "ip_notes");
define("SQL_BAN_APPEALS", "ban_appeals");
define("SQL_JANITOR_APPS", "janitor_apps");
define("SQL_JANITOR_APPS_BACKUP", "janitor_apps_backup");
define("SQL_BLOTTER_MESSAGES", "blotter_messages");
define("SQL_FEEDBACK", "feedback");
define("SQL_MOD_ACTIONS","mod_actions");

/***Board Settings***/

// board
define("FORCED_ANON", false); // Removes the ability to set names
define("NSFW", true); // Determines if the board uses the blue or red theme as a default.
define("ARCHIVE", true); // Determines if the board archives post after pruning
define("SPOILERS", false); // Determines if the board allows spoilers (text/image)
define("NO_FILE", false); // Determines if users can make threads without a file
define("DESCRIPTION", "An anoymous imagboard.");

// pages
define("PAGES_THREADS", 10); // The amount of threads-per-page
define("PAGES_BOARD", 5); // The amount of threads-per-page

// comment
define("MAX_LINES", 50); // Max line breaks
define("MAX_CHAR", 2000); // Max number of characters in a single comment

// cooldown
define("COOLDOWN_THREAD", 600); // Time in seconds before a user can make another thread
define("COOLDOWN_REPLY", 60); // Time in seconds before a user can reply to another post
define("COOLDOWN_FILE", 30); // Time in seconds before a user can post another file

// threads
define("BUMP_LIMIT", 50); // The amount of replies before a thread autosages

// delete
define("DELETE_EARLY", 60); // Time in seconds before a user can delete a post
define("DELETE_OLD", 3600); // Time in seconds before deleting a post becomes invalid

// Image Settings
define("MAX_KB", 4028);
define("THUMB_WIDTH", 250); // Max width of image before it thumbnails
define("THUMB_HEIGHT", 250); // Max height of image before it thumbnails

// Recaptcha Settings
define("RECAPTCHA", true);
define("SITE_KEY", "");
define("SECRET_KEY", "");

/***Admin Settings***/

// URLS
define("SITE_ROOT", "localhost:63342");
define('SITE_SUFFIX', preg_replace('/^.*\.(\w+)$/', '\1', SITE_ROOT));
define('BOARD_DIR', basename(__DIR__));
define("PHP_SELF", "imgboard.php");
define('SITE_ROOT_BD', '//' . SITE_ROOT.'/' . BOARD_DIR);
define('PHP_SELF_ABS', '//'. SITE_ROOT_BD.'/' . PHP_SELF);
define('CSS_PATH', '//' . SITE_ROOT_BD . '/css/');