<?php


class Head
{
    public $info = [
        "page" => [
            "title" => "/trash/ - On-Topic",
            "sub" => ""
        ],
        "js" => [
            "raw" => [],
            "script" => []
        ]
    ];

    function generate()
    {
        $head = "<!DOCTYPE html><head>";
        $head .= "<meta charset='utf-8'>";
        $head .= "<title>" . strip_tags($this->info['page']['title']) . "</title>";
        $head .= "<meta name='description' content='" . DESCRIPTION . "'>";
        $head .= "<meta name='robots' content='noarchive'>";
        $head .= "<meta name='keywords' content='imageboard,4chan,meme,anonymous,technology,discussion'>";
        $head .= "</head>";
        $head .= "<body>";

        return $head;
    }
}