<?php

// TODO: Configure board/thread input values

include_once("..\..\strings.php");
include_once("..\..\config.php");

class PostForm
{
    function generate()
    {
        $max_byte = MAX_KB * 1024;
        $form = "";
        $form .= "<form name='post' action='" . PHP_SELF_ABS . "' method='post' enctype='multipart/form-data'>";
        $form .= "<input type='hidden' name='MAX_FILE_SIZE' value='" . $max_byte . "'>";
        $form .= "<table class='postForm' id='postForm'>";
        $form .= "<tbody>";

        // Name
        if (!FORCED_ANON)
        {
            $form .= "<tr data-type='Name'><td>" . S_NAME . "</td><td><input name='name' type='text' tabindex='1' placeholder=" . S_ANON . "></td></tr>";
        }

        $form .= "<tr data-type='Options'><td>" . S_EMAIL . "</td><td><input type='text' name='email' tabindex='2'>";
        $form .= "<tr data-type='Subject'><td>" . S_SUB . "</td><td><input type='text' name='email' tabindex='3'>";
        $form .= "<input type='submit' value='" . S_SUBMIT . "'></td></tr>";
        $form .= "<tr data-type='Comment'><td>" . S_COM . "</td><td><textarea name='com' cols='48' rows='4' wrap='soft' tabindex='4'></textarea></td></tr>";

        $form .= "<tr data-type='File'><td>" . S_FILE . "</td><td><input type='file' name='upfile' tabindex='7'>";

        $form .= "</tbody>";
        $form .= "<tfoot><tr><td colspan='2'><div id='postFormError'></div></td></tr></tfoot>";
        $form .= "</table></form>";

        return $form;
    }
}