<?php

require_once("../../config.php");

class  Recaptcha
{
    function is_valid()
    {
        return $this->validate();
    }

    private function validate()
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = [
            "secret" => SECRET_KEY,
            "response" => $_POST['g-recaptcha-response'],
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        ];
        $headers = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => "POST",
                'content' => http_build_query($data),
            ]
        ];
        $context  = stream_context_create($headers);
        $result = file_get_contents($url, false, $context);
        $result = json_decode($result);

        return $result->success;
    }
}